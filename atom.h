
#ifndef ATOM_H
#define ATOM_H

#include <string>
#include <iostream>

using namespace std;

struct atom{
	string name;
	double r[3];
	double q; //the charge
	double index; //the index for what type it is
	
	friend ostream &operator<<(ostream &os,const atom &at);
	friend istream &operator>>(istream &is,atom &at); 
	friend bool operator==(atom &a1,atom &a2);
};
#endif
