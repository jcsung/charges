#include "atom.h"
#include <string>
#include <iostream>

using namespace std;

ostream &operator<<(ostream &os, const atom &at){
	os<<at.name;
	os<<' ';
	for (int i=0; i<3; i++){
		os<<at.r[i]<<' ';
	}
	os<<' '<<at.q<<' '<<at.index;
	return os;
}

istream &operator>>(istream &is, atom &at){
	is>>at.name;
	for (int i=0; i<3; i++){
		is>>at.r[i];
	}
	at.q=0;
	at.index=-1;
	return is;
}

bool operator==(atom &a1, atom &a2){
	return a1.name==a2.name;
}
