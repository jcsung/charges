#include <iostream>
#include <fstream>
#include "atom.h"
#include "util.h"
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;
using namespace util;

double TOLERANCE=0.00001;
double Qxx=0.336;
double Qyy=-0.673;
double Qzz=0.336;

double Qij(const vector <atom> &sys, int i, int j);
bool isValid(const vector <atom> &sys);
double err(const vector <double> &quad);

double err(const vector <double> &quad){
	double val;
	double err=0;
	val=(Qxx-quad[0]);
	err+=val*val; 
	val=(Qyy-quad[4]);
	err+=val*val;
	val=(Qzz-quad[8]);
	err+=val*val;
	return err;
}

double Qij(const vector <atom> &sys,int i, int j){
	double Q=0;
	int l, x;
	double r;
	for (l=0; l<sys.size(); l++){
		//Determine the squared magnitude of the current position vector
		r=0;
		for (x=0; x<3; x++){
			r+=sys[l].r[x]*sys[l].r[x];
		}
		//Add to the Q_ij
		Q+=sys[l].q*(3*sys[l].r[i]*sys[l].r[j]-r*(i==j?1:0));
	}
	return Q;
}

bool isValid(const vector <atom> &sys){
	double sum=0;
	bool flag=false;
	for (int x=0; x<sys.size(); x++){
		sum+=sys[x].q;	
	}
	if (sum*sum<TOLERANCE*TOLERANCE){
		flag=true;
	}
	return flag;
}

int main(){
	
	
	string guest="ethane.xyz";
	string limfil="limits.in";
	vector <atom> sys;
	int x, y, size;

	//1. Read the ethane.xyz file
	ifstream infile(guest.c_str());
	infile>>size;
	infile.ignore(100,'\n'); //the second line of xyz
	sys.resize(size);
	for (x=0; x<size; x++){
		infile>>sys[x];
	}
	infile.close();

/*
	//DEBUG: Write out ethane.xyz
	for (x=0; x<size; x++){
		cout<<sys[x]<<endl;
	}
*/

	//2. Read in the limits
	double C_MIN, C_MAX, C_INC, H_MIN, H_MAX, H_INC, XCH_MIN, XCH_MAX, XCH_INC, XCC_MIN, XCC_MAX, XCC_INC;
	infile.open(limfil.c_str());
	infile>>C_MIN>>C_MAX>>C_INC;
	infile>>H_MIN>>H_MAX>>H_INC;
	infile>>XCH_MIN>>XCH_MAX>>XCH_INC;
	infile>>XCC_MIN>>XCC_MAX>>XCC_INC;
	infile.close();
	cout<<"Search Parameters:"<<endl;
	cout<<"C atomic charge: ["<<C_MIN<<","<<C_MAX<<"], increments of "<<C_INC<<endl;
	cout<<"H atomic charge: ["<<H_MIN<<","<<H_MAX<<"], increments of "<<H_INC<<endl;
	cout<<"C-H bond charge: ["<<XCH_MIN<<","<<XCH_MAX<<"], increments of "<<XCH_INC<<endl;
	cout<<"C-C bond charge: ["<<XCC_MIN<<","<<XCC_MAX<<"], increments of "<<XCC_INC<<endl;
	cout<<"Tolerance for mathematical comparisons: "<<TOLERANCE<<endl;

	double c, h, xch, xcc;
	double val;
	bool flag;
	double error;
	int index;
	vector <double> quad(9);

	double min=100;
	vector <double> parm(4);	

	//3. Iterate through values
	for (c=C_MIN; c<=C_MAX; c+=C_INC){
		for (h=H_MIN; h<=H_MAX; h+=H_INC){
			for (xch=XCH_MIN; xch<=XCH_MAX; xch+=XCH_INC){
				for (xcc=XCC_MIN; xcc<=XCC_MAX; xcc+=XCC_INC){
					//cout<<c<<" "<<h<<" "<<xch<<" "<<xcc<<endl;	
				
					//4. Set the charges
					for (x=0; x<sys.size(); x++){
						if (sys[x].name=="C"){
							sys[x].q=c;
						}
						else if(sys[x].name=="H"){
							sys[x].q=h;
						}
						else if(sys[x].name=="XCH"){
							sys[x].q=xch;
						}
						else if(sys[x].name=="XCC"){
							sys[x].q=xcc;
						}
						else{
							sys[x].q=0;
							cout<<"Warning: invalid atom type found"<<endl;
						}
					}

					//5. Verify valid charge set
					flag=isValid(sys);
					if (!flag) continue;	
	
					//6. Calculate quadrupole moment tensor
					index=0;
					for (x=0; x<3; x++){
						for (y=0; y<3; y++){
							//cout<<"Q["<<x+1<<"]["<<y+1<<"]="<<Qij(sys,x,y)<<" ";
							quad[index]=Qij(sys,x,y);
							if (quad[index]*quad[index]<TOLERANCE*TOLERANCE) quad[index]=0;
							//cout<<quad[index++]<<" ";
							index++;
						}
						//cout<<endl;
					}
					//7. Check how close it is to experiment
					error=err(quad);
					if (error<min){
						min=error;
						parm[0]=c;
						parm[1]=h;
						parm[2]=xch;
						parm[3]=xcc;
					}

					//Some output stuff
					//cout<<c<<" "<<h<<" "<<xch<<" "<<xcc<<" "<<error<<endl;	
					//cout<<endl;
				}
			}
		}
	}

	cout<<endl<<"Best Parameter Set: "<<endl;
	cout<<"C atomic charge: "<<parm[0]<<endl;
	cout<<"H atomic charge: "<<parm[1]<<endl;
	cout<<"C-H bond charge: "<<parm[2]<<endl;
	cout<<"C-C bond charge: "<<parm[3]<<endl;
	cout<<"Quadrupole Moment Tensor: "<<endl;
	for (x=0; x<sys.size(); x++){
		if (sys[x].name=="C"){
			sys[x].q=parm[0];
		}
		else if(sys[x].name=="H"){
			sys[x].q=parm[1];
		}
		else if(sys[x].name=="XCH"){
			sys[x].q=parm[2];
		}
		else if(sys[x].name=="XCC"){
			sys[x].q=parm[3];
		}
		else{
			sys[x].q=0;
			cout<<"Warning: invalid atom type found"<<endl;
		}
	}
	index=0;
	for (x=0; x<3; x++){
		for (y=0; y<3; y++){
			quad[index]=Qij(sys,x,y);
			if(quad[index]*quad[index]<TOLERANCE*TOLERANCE) quad[index]=0;
			cout<<quad[index]<<" ";
			index++;
		}
		cout<<endl;
	}
	cout<<"Error: "<<err(quad)<<endl;
	return 0;
}
