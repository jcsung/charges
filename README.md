A quick and dirty program written while studying the charge distribution of 
electrons in the hydrocarbon ethane.  

The goal of this quick examination was to determine how point charges in 
ethane would need to be distributed such that they could correctly reproduce 
the experimental quadrupole moment (as given by the NIST CCCBDB).

Point charges were placed at the atomic Carbon (C) and Hydrogen centers (H) 
as well as at the Carbon-Hydrogen bond midpoint (XCH) and the Carbon-Carbon 
bond midpoint (XCC).

To carry out the study, a grid search over the paramter space was carried out, 
with the limits (found in limits.in) chosen by chemical intuition.
